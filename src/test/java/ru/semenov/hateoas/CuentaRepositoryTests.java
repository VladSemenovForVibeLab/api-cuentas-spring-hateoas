package ru.semenov.hateoas;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import ru.semenov.hateoas.model.Cuenta;
import ru.semenov.hateoas.repository.CuentaRepository;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Rollback(value = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CuentaRepositoryTests {
    @Autowired
   private CuentaRepository cuentaRepository;
    @Test
    void testAgregarCuenta(){
        Cuenta cuenta = new Cuenta(1231,"1000");
        Cuenta cuentaGuardada = cuentaRepository.save(cuenta);
        assertThat(cuentaGuardada).isNotNull();
        assertThat(cuentaGuardada.getId()).isGreaterThan(0);
    }
}
