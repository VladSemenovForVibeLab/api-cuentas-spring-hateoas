package ru.semenov.hateoas.service;

import jakarta.persistence.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.hateoas.exception.CuentaNotFoundException;
import ru.semenov.hateoas.model.Cuenta;
import ru.semenov.hateoas.repository.CuentaRepository;

import java.util.List;

@Service
@Transactional
public class CuentaService {
    @Autowired
    private CuentaRepository cuentaRepository;
    public List<Cuenta> listAll(){
        return cuentaRepository.findAll();
    }
    public Cuenta get(Integer id){
        return cuentaRepository.findById(id).get();
    }
    public Cuenta save(Cuenta cuenta){
        return cuentaRepository.save(cuenta);
    }
    public void delete(Integer id) throws CuentaNotFoundException {
        if(!cuentaRepository.existsById(id)){
            throw new CuentaNotFoundException("Cuenta no encontrada con el ID : "+ id);
        }
        cuentaRepository.deleteById(id);
    }
    public Cuenta depositar(float monto,Integer id){
        cuentaRepository.actializarMonto(monto,id);
        return cuentaRepository.findById(id).get();
    }
    public Cuenta retitar(float monto,Integer id){
        cuentaRepository.actializarMonto(-monto,id);
        return cuentaRepository.findById(id).get();
    }
}
