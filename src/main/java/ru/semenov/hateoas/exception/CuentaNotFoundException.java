package ru.semenov.hateoas.exception;

public class CuentaNotFoundException extends Exception{
    public CuentaNotFoundException(String message){
        super(message);
    }
}
