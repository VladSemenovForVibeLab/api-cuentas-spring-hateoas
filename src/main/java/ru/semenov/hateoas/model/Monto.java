package ru.semenov.hateoas.model;

import lombok.Data;

@Data
public class Monto {
    private float monto;
}
